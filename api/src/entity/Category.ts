import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Theme } from "./Theme";

@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    reference: string;

    @Column()
    name: string;

    @Column("text")
    description: string;    

    @Column()
    active: boolean;

    @ManyToOne(type => Theme, theme => theme.categories)
    theme: Theme[];    

}

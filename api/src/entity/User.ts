import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, AfterInsert, AfterUpdate, AfterRemove} from "typeorm";
import {IsEmail, IsNotEmpty, IsDateString, IsInt} from "class-validator";

const crypt = require('crypto');

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @IsNotEmpty()
    @Column()
    firstName: string;

    @IsNotEmpty()
    @Column()
    lastName: string;

    @IsDateString()
    @Column("timestamp")
    birthday: Date;

    @IsEmail()
    @Column()
    email: string;

    @IsNotEmpty()
    @Column()
    password: string;

    @Column()
    salt: string;

    @Column()
    role: string;

    @Column()
    token: string;

    @Column("timestamp")
    createdAt: Date;

    @Column("timestamp")
    updateAt: Date;

    @Column()
    active: boolean;

    @BeforeInsert()
    async  init() {
        this.salt = await Math.random().toString(36);
        console.log(this.salt);
        this.password = await crypt.createHmac('sha512', this.salt).update(this.password).digest('hex');
        this.createdAt =  new Date();
    }

    @BeforeInsert()
    @BeforeUpdate()
    async update(){
        let slatBis = await new Date().getUTCMilliseconds() + "to do cryptage";
        this.token = await crypt.createHmac('sha512', this.salt).update(slatBis).digest('hex');
        this.updateAt =  new Date();
    }
    @AfterInsert()
    @AfterUpdate()
    @AfterRemove()
    async publicUser(){
        delete this.password;
        delete this.salt;
    }
}

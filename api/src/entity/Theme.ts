import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Category } from "./Category";

@Entity()
export class Theme {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    reference: string;

    @Column()
    name: string;

    @Column("text")
    description: string;    

    @Column()
    active: boolean;

    @OneToMany(type => Category, category => category.theme)
    categories: Category[];

}

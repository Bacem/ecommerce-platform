import "reflect-metadata";
import {createConnection} from "typeorm";
const fs = require('fs');

async function loadEntity(name:any){
    let allData = require('./data/'+name+'.json'); 
    let query; 
    query = "-- Fixture: "+name+" \n\n";
    //query = query+"DROP TABLE "+name+" \n;";
    for (let object of allData) {
        let keyObject = Object.keys(object);
        let values:any[] = [];
        let column:any[] = [];
        for (let key of keyObject) {
            let contentColumn;
            let contentValues;
            if(key === "key"){
                let currentKey = Object.keys(object[key])[0]; 
                contentColumn = "\""+currentKey+"Id\"";
                contentValues = object[key][currentKey];
            }else{
                contentColumn = "\""+key+"\"";
                contentValues = object[key];                    
            }
            if(typeof contentValues !== 'number'){
                contentValues = "'"+contentValues+"'";
            }               
            column.push(contentColumn);
            values.push(contentValues);                
        }
        query = query+"INSERT INTO public.\""+name+"\"("+column.toString()+") VALUES ("+values.toString()+"); \n"
    }
    query = query+"\n";
    return query;
}

async function writeSql(){
    console.log("Start Generate Fixture ...");
    let sql = ""; 
    let dropSql = "-- Drop all table \n -- DROP TABLE " ;
    let planning = require('./planning.json');
    for (let entityName of planning) {
        console.log("> Table : "+entityName);
        sql = sql + await loadEntity(entityName);
        dropSql = dropSql + entityName+", ";
    } 
    dropSql = dropSql.slice(0, -2);
    sql = dropSql+"; \n"+ sql 
    fs.writeFile(__dirname+'/fixture.sql', sql, (err) => {
        if (err) throw err;
        console.log('The SQL has been created in fixture.sql');
        });   
} 
writeSql();
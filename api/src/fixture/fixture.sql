-- Drop all table 
 -- DROP TABLE theme, category, user; 
-- Fixture: theme 

INSERT INTO public."theme"("id","reference","name","description","active") VALUES (1,'Jeux','Jeux','Voir la vitrine Jeux Jouet','true'); 
INSERT INTO public."theme"("id","reference","name","description","active") VALUES (2,'electromenager','Electroménager','Voir la vitrine electroménager','true'); 
INSERT INTO public."theme"("id","reference","name","description","active") VALUES (3,'meuble','Meuble canapé','Voir la vitrine meuble','true'); 
INSERT INTO public."theme"("id","reference","name","description","active") VALUES (4,'jardin','Jardin Brico','Voir la vitrine jardin','true'); 
INSERT INTO public."theme"("id","reference","name","description","active") VALUES (5,'informatique','Informatique','Voir la vitrine informatique','true'); 

-- Fixture: category 

INSERT INTO public."category"("id","reference","name","description","active","themeId") VALUES (1,'3d','Jeux 3D','Voir la vitrine Jeux 3D','true',1); 
INSERT INTO public."category"("id","reference","name","description","active","themeId") VALUES (2,'jeux-tirs','Jeux de tirs - Nerf','Voir la vitrine Jeux de tirs - Nerf','true',1); 
INSERT INTO public."category"("id","reference","name","description","active","themeId") VALUES (3,'pc-portable','Pc Portable','Voir la vitrine Pc Portable','true',5); 
INSERT INTO public."category"("id","reference","name","description","active","themeId") VALUES (4,'pc-bureau','PC de bureau','Voir la vitrine PC de bureau','true',5); 
INSERT INTO public."category"("id","reference","name","description","active","themeId") VALUES (5,'Processeur','Processeurs','Voir la vitrine Processeurs','true',5); 

-- Fixture: user 

INSERT INTO public."user"("id","firstName","lastName","age","email","role","password","active") VALUES (1,'dedie','loomed',26,'dedier@gmail.com','ROLE_USER','admin','true'); 


import {Service} from "typedi";
const emailingApi = require("sendinblue-api");
const appConfig = require('./../config/appConfig.json');

@Service()
export class Emailing{
    validAccount(toEmail, toName, token) {
        let to = '{"'+toEmail+'":"'+toName+'"}';
        var parameters = { "apiKey": appConfig.emailing.key, "timeout": 5000 };
        var emailingObj = new emailingApi(parameters);
        var input =	{
            'to': JSON.parse(to),
            'from': [ appConfig.emailing.fromEmail, appConfig.emailing.fromName],
            'subject': 'Test mail form sendinblue',
            'html': '<a href="'+appConfig.frontend.host+'/user/active/'+token+'">active you account</a>'
        };
        emailingObj.send_email(input, function(err, response){
            if(err){
                console.log(err);
            } else {
                console.log(response);
            }
        });
    }
}
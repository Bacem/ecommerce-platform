import "reflect-metadata";
import {createConnection } from "typeorm";
import {createExpressServer, Action} from "routing-controllers";
import {ThemeController} from "./controller/ThemeController";
import {UserController} from "./controller/UserController";

const jwt = require('jsonwebtoken');
const appConfig = require('./config/appConfig.json');
const ormConfig = require('./config/ormConfig.json');


// Management authorization
const app = createExpressServer({
    authorizationChecker: async (action: Action, roles: string[]) => {
        let token = await  action.request.headers["authorization"];
        if(token.split(' ')[0] === "Bearer"){
            token = token.split(' ')[1];
            let decoded = jwt.verify(token, appConfig.security.jwt.secret);
            let keyJwt = Object.keys(decoded);
            if ( decoded && keyJwt[0] === "email" && keyJwt[1] === "role" && roles.indexOf(keyJwt[1]) !== -1 ){
                return true
            }else{
                return false
            }
        }else {
            return false
        }
    },
    cors: true,
    controllers: [
        ThemeController,
        UserController
    ]
});


// Up server
app.listen(appConfig.server.port);
console.log(" > Express server has started on port "+appConfig.server.port);

// Connexion Db
createConnection(ormConfig).then(async connection => {
    console.log(" > Connected To database "+ormConfig.database);
}).catch(error => console.log(error));
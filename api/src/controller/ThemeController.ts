import { Controller, Param, Body, Get, Post, Put, Delete} from "routing-controllers";
import { Container } from "typedi";
import { getRepository } from "typeorm";
import { Emailing } from "../service/Emailing";
import { Theme } from "../entity/Theme";

@Controller()
export class ThemeController {

    private themeRepository = getRepository(Theme);

    @Get("/themes")
    getAll() {
        return this.themeRepository.find({ active:true });
    }

    @Get("/themes/:id")
    getOne(@Param("id") id: number) {
       return "This action returns theme #" + id;
    }

    @Post("/themes")
    post(@Body() theme: any) {
        console.log(theme);
       return "Saving theme...6666";
    }

    @Put("/themes/:id")
    put(@Param("id") id: number, @Body() theme: any) {
        console.log(id, theme);
       return "Updating a theme...";
    }

    @Delete("/themes/:id")
    remove(@Param("id") id: number) {
        console.log(id);
       return "Removing theme...";
    }

}
import {JsonController, Param, Body, Get, Post, Put, Delete, NotFoundError, BadRequestError} from "routing-controllers";
import {getRepository} from "typeorm";
import { Emailing } from "../service/Emailing";
import { User } from "../entity/User";
import {Container} from 'typedi';

const crypt = require('crypto');
const jwt = require('jsonwebtoken');
const appConfig = require('./../config/appConfig.json');

@JsonController("/user")
export class UserController {

    private userRepository = getRepository(User);

    /**
     *
     * @param {User} user
     * @returns {Promise<User>}
     */
    @Post("/register")
    async register(@Body() user: User) {
        user.role = "ROLE_USER";
        let currentDate = new Date();
        let birthday = new Date(user.birthday);
        if( birthday > currentDate){
            throw new BadRequestError("Birthday date not valid");
        }
        // User is Exist
        let isExist = await await this.userRepository.findOne({ email: user.email });
        if(isExist){
            throw new BadRequestError("Email used by anther user.");
        }
        user.active = false;
        user = await this.userRepository.save(user);
        let emailing = Container.get(Emailing);
        emailing.validAccount(user.email, user.firstName+" "+user.lastName, user.token);
        return user;
    }

    /**
     *
     * @param {string} token
     * @returns {Promise<boolean>}
     */
    @Get("/active/:token")
    async active(@Param("token") token: string) {
        let user = await this.userRepository.findOne({ token: token });
        console.log(user.token);
        if( !user ){
            throw new BadRequestError("This user not found.");
        }
        user.active = true;
        await this.userRepository.save(user);
        return "Your account activate, you can connect";
    }

    /**
     *
     * @param data
     * @returns {Promise<{firstName: any | string; lastName: any | string; email: string | any; token: any}>}
     */
    @Post("/login")
    async login(@Body() data: any) {
        let user = await await this.userRepository.findOne({ email: data.email });
        if( user ) {
            let password = await crypt.createHmac('sha512', user.salt).update(data.password).digest('hex');
            if( password === user.password ){
                let token = jwt.sign({ "email": user.email, "role" : user.role }, appConfig.security.jwt.secret);
                return {
                    "firstName": user.firstName,
                    "lastName": user.lastName,
                    "email": user.email,
                    "token": token
                }
            }else{
                throw new BadRequestError("password not valid");
            }
        }else {
            throw new BadRequestError("This email not found");
        }
    }

    @Put("/users/:id")
    put(@Param("id") id: number, @Body() user: any) {
        console.log(id, user);
       return "Updating a user...";
    }

    @Delete("/users/:id")
    remove(@Param("id") id: number) {
        console.log(id);
       return "Removing user...";
    }

}
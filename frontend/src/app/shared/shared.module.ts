import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from 'clarity-angular';
import { AlertComponent } from './alert/alert.component';

@NgModule({
    imports: [
        CommonModule,
        ClarityModule
    ],
    declarations: [
        AlertComponent
    ],
    exports: [
        AlertComponent
    ]
})
export class SharedModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RegisterService {

    constructor(
        private http: HttpClient,
    ) { }

    register(data): Observable<any> {
        return this.http.post('http://localhost:3000/user/register', data);
    }

}

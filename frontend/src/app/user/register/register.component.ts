import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    typeAlert: string;
    textText: string;
    showForm: boolean;
    constructor(
        private registerService: RegisterService
    ) { }

  ngOnInit() {
      this.showForm = true;
  }
  onSubmit(registerForm) {
    registerForm.value.birthday = new Date(registerForm.value.birthday);
    this.registerService.register(registerForm.value).subscribe(
        data => {
            console.log(data);
            this.typeAlert = 'success';
            this.textText = 'Your account is well created, <br>Check your email and validate your account';
            this.showForm = false;
        },
        errorRegister => {
            this.typeAlert = 'danger';
            this.textText = this.textText = errorRegister.error.message;;
        }
    );
   }
}

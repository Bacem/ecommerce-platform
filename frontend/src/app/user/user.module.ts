import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from 'clarity-angular';
import { CookieModule} from 'ngx-cookie';


import { UserRoutingModule } from './user-routing.module';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';
import { LoginService } from './login/login.service';
import { RegisterService } from './register/register.service';
import { CoreModule } from '../core/core.module';
import {AppService} from '../app.service';

@NgModule({
  imports: [
      CommonModule,
      ClarityModule,
      FormsModule,
      SharedModule,
      CookieModule.forChild(),
      CoreModule,
      UserRoutingModule
  ],
  declarations: [
      AccountComponent,
      LoginComponent,
      RegisterComponent
  ],
  providers: [
      LoginService,
      RegisterService,
      AppService
  ]
})
export class UserModule { }

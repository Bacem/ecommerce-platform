import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
  constructor(
   private http: HttpClient,
  ) { }

    /**
     * @param data
     * @returns {Observable<any>}
     */
      login(data): Observable<any> {
          return this.http.post('http://localhost:3000/user/login', data);
      }

    /**
     * @param token
     * @returns {Observable<any>}
     */
    active(token): Observable<any> {
        return this.http.get('http://localhost:3000/user/active/' + token );
    }
}

import { Injectable  } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AppService {
   private space = new Subject<any>();
   spaceObservable$ = this.space.asObservable();
  constructor() { }
  public setSpace(data: any) {
      if (data) {
          this.space.next(data);
      }
  }
}
